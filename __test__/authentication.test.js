require('dotenv').config()
const request = require("supertest");
const app = require("../app");

describe("Authentication Testing", () => {

  // let AKSES;
  let akses;

  beforeAll(() => {
    // AKSES = request(app)
    //   .post("/v1/auth/login")
    //   .send({
    //     email: "johnny@binar.co.id",
    //     password: "123456"
    //   })
    //   .then((res) => {
    //     console.log(res.body.accessToken, "INI AKSES TOKEENNNNN")
    //     return res.body.accessToken;
    //   });

    akses = request(app)
      .post("/v1/auth/login")
      .send({
        email: "jansen@mail.com",
        password: "090900"
      })
      .then((res) => {
        console.log(res.body.accessToken, "ini akses tokeennnnn")
        return res.body.accessToken;
      });
    })

  it("should response with 201 as status code", async () => {
    const email = "johnny@binar.co.id";
    const password = "123456";
    let data = {
      email: email,
      password: password
    }

    return request(app)
      .post("/v1/auth/login")
      .set("Content-Type", "application/json")
      .send(data)
      .then((res) => {
        expect(res.statusCode).toBe(201);
        expect(res.body).toEqual(res.body);
        // expect(res.body).toEqual(
        //   expect.objectContaining({
        //     accessToken: expect.any(String),
        //   })
        // );
        
      });
  });

  it("should response with 404 as status code", async () => {
    const email = "johnny@binar";
    const password = "123450";
    let data = {
      email: email,
      password: password
    }

    return request(app)
      .post("/v1/auth/login")
      .set("Content-Type", "application/json")
      .send(data)
      .then((res) => {
        expect(res.statusCode).toBe(404);
        expect(res.body).toEqual(res.body);
        // expect(res.body).toEqual(
        //   expect.objectContaining({
        //     accessToken: expect.any(String),
        //   })
        // );
        
      });
  });

  it("should response with 401 as status code", async () => {
    const email = "johnny@binar.co.id";
    const password = "salah";
    let data = {
      email: email,
      password: password
    }

    return request(app)
      .post("/v1/auth/login")
      .set("Content-Type", "application/json")
      .send(data)
      .then((res) => {
        expect(res.statusCode).toBe(401);
        expect(res.body).toEqual(res.body);
        // expect(res.body).toEqual(
        //   expect.objectContaining({
        //     accessToken: expect.any(String),
        //   })
        // );
        
      });
  });

  it("should response with 201 as status code, kalau akun berhasil dibuat", async () => {
    const email = "jansen5@mail.com"; // diganti terus emailnya soalnya pas ditest kebuat juga di database.
    const password = "090900";
    let data = {
      email: email,
      password: password
    }

    return request(app)
      .post("/v1/auth/register")
      .set("Content-Type", "application/json")
      .send(data)
      .then((res) => {
        expect(res.statusCode).toBe(201);
        expect(res.body).toEqual(res.body);
        
      });
  });

  it("should response with 422 as status code, kalau akun ternyata sudah ada", async () => {
    const email = "jansen@mail.com";
    const password = "090900";
    let data = {
      email: email,
      password: password
    }

    return request(app)
      .post("/v1/auth/register")
      .set("Content-Type", "application/json")
      .send(data)
      .then((res) => {
        expect(res.statusCode).toBe(422);
        expect(res.body).toEqual(res.body);
        
      });
  });

  it("should response with 200 as status code, kalau token benar di WhoAmI dan usernya CUSTOMER", async () => {
    
    return request(app)
      .get("/v1/Auth/whoami") // get jangan post
      .set("Content-Type", "application/json")
      .set("Authorization", `Bearer ${await akses}`)
      .then((res) => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(res.body);
      });
  });

  // it("should response with 200 as status code, kalau token benar di WhoAmI dan usernya ADMIN", async () => {
    
  //   return request(app)
  //     .get("/v1/Auth/whoami") // get jangan post
  //     .set("Content-Type", "application/json")
  //     .set("Authorization", `Bearer ${AKSES}`)
  //     .then((res) => {
  //       expect(res.statusCode).toBe(200);
  //       expect(res.body).toEqual(res.body);
  //     });
  // });

  it("should response with 401 as status code, kalau token tidak benar di WhoAmI", async () => {

    let token = "token_tidak_benar"
    
    return request(app)
      .get("/v1/Auth/whoami") // get jangan post
      .set("Content-Type", "application/json")
      .set("Authorization", `Bearer ${token}`)
      .then((res) => {
        expect(res.statusCode).toBe(401);
        expect(res.body).toEqual(res.body);
      });
  });
  




});
