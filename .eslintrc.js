module.exports = {
    "env": {
        "node": true,
        "commonjs": true,
        "es2021": true,
        "jest": true // https://www.reddit.com/r/reactjs/comments/gpwvdx/jest_reference_error_describe_is_not_defined_when/ , supaya describe it expect ga error
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": "latest"
    },
    "rules": {
    }
}
